/**************************************************************************/
/*! 
    @file     readFelicaUid.pde
    @author   Andrew King (CrossroadsFoundation)
	@license  BSD (see license.txt)

    This is based on readMelica.pde example from Adafruit.
    It will read a UID from a FELICA based card and output it to the keyboard of 
    a connected computer, in the form ###1515a3e3091a
    Requires Keyboard library (standard)
    Requires Adafruit_PN532 library, (Crossroads version!)
    Will only run on devices with Keyboard support (not Uno!)
    ie. Leonardo, Due

*/
/**************************************************************************/

#include <Adafruit_PN532.h>

#define SCK  (2)
#define MOSI (3)
#define SS   (4)
#define MISO (5)

Adafruit_PN532 nfc(SCK, MISO, MOSI, SS);

void setup(void) 
{
  Serial.begin(9600);
  // while (!Serial);
  // Serial.println("Hello!");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (!versiondata) 
  {
    Serial.print("Didn't find PN53x board");
    
    while (1) 
    {
      digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(100);               // wait for a second
      digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
      delay(100);  
    }
  }
  // Got ok data, print it out!
  // Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  // Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  // Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  Keyboard.begin();
  
  // Serial.println("Waiting for a FeliCa Card ...");
}

uint8_t old_uid[] = { 0, 0, 0, 0, 0, 0, 0, 0 };

void loop() 
{
  const uint8_t expectedUidLength = 8;
  uint8_t success = 0;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength = 0;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
    
  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_FELICA_FAST, uid, &uidLength);
  
  if (success) 
  {
    if (uidLength != expectedUidLength)
      return;
      
    int res = memcmp(old_uid, uid, expectedUidLength);
    if (res == 0)
    {
      // same card. 
      // just delay
      // Serial.println("Same card detected, waiting 3 seconds");
      // reset old_uid so we can detect same card again after some time.
      memset(old_uid, 0, expectedUidLength);
      delay(2500);
    }
    else
    {
      char chars[16]; // ### + 4 + 8 + null termination
      snprintf(chars, 16, "###%01x%01x%01x%01x%02x%02x%02x%02x", uid[0], uid[1], uid[2], uid[3], uid[4], uid[5], uid[6], uid[7]);
      Keyboard.println(chars);

      memcpy(old_uid, uid, expectedUidLength);
      
      delay(100);
    }
  }
}

